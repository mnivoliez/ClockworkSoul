# Characters

## Tara

- Main protagonist
- Possessed by the player
- Care for her brother and will do anything to protect him
- Adversity made her strong 

## Clauss

- Tara's brother
- Can be ordered around by the player
  - come
  - stay hidden
  - go there
- Young and of fragile constitution
- Love her sister, will not hesitate to sacrifice himself if necessarry

## Arch Bishop

- Main antagonist
- Is the head of the church
- Want to create a new god, whatever the cost
- No sense of empathy
- As any other member of the church, body heavily augmented with biomechanical part.

## Priest

- More common enemies in the game
- Face not visible
- Body augmented to the point where we don't know if it's a machine with body part or the other way around.
- Relentless
- They feel constant pain
- Will chase Tara and Clauss whenever they see them.


