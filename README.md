# Clockwork Soul (temporary name)

## Overview

"When mankind failed to find god, they decide to create one..."

The game feature 2 protagonist (one being the player) trying to escape from a dangerous cult.
The environment of the game is futurist.

## Concept

The player must escape while protecting its companion.
The environment will be used for cover and secrecy.
The focus will be on tension and athmosphere.

## Rules

- The player can move anywhere on the map.
- The player's companion can move like the player but in limited version:
  - Run slower
  - Jump smaller
- The player must not been seen by AI.
- The player must reach the targeted safe zone to win the level.
- Only two safe zone per level:
  - Spawn
  - Target zone

## Story

## [Levels](Docs/Levels.md)

## [Characters](Docs/Characters.md)

## Requirements

- SFX:
  - Humans walking
  - Mechanic gear
  - Creature breathing
  - Mechanism sound
- Texture:
  - To be defined
- Music:
  - Choice has been made to not set up any music in game.

